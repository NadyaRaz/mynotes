package com.example.mynotes.presentation.feature.createnote.fragmentview;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.mynotes.R;
import com.example.mynotes.dto.NoteModel;

public class CreateNoteFragmentView extends ConstraintLayout implements ICreateNoteFragmentView {

    private EditText createNoteText;
    private EditText createNoteTitle;
    private ImageView createNotePhoto;
    private ImageView createNoteAttachPicture;
    private Listener listener;
    private Toolbar createNoteToolbar;
    private ImageView deletePhotoImageView;

    private String uri;

    public CreateNoteFragmentView(Context context) {
        super(context);
    }

    public CreateNoteFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CreateNoteFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
    }

    private void findViews() {
        createNoteTitle = findViewById(R.id.create_note_title);
        createNoteText = findViewById(R.id.create_note_text);
        createNotePhoto = findViewById(R.id.create_note_image_view);
        createNoteAttachPicture = findViewById(R.id.create_note_attach_image_view);
        createNoteToolbar = findViewById(R.id.create_note_toolbar);
        deletePhotoImageView = findViewById(R.id.create_note_delete_photo);
    }

    @Override
    public void populate(NoteModel model) {
        if (model.getTitle() != null) {
            createNoteTitle.setText(model.getTitle());
        }
        if (model.getText() != null) {
            createNoteText.setText(model.getText());
        }
        if (model.getPictureUri() != null) {
            setImage(Uri.parse(model.getPictureUri()));
        }
    }

    @Override
    public void initToolbar() {
        createNoteToolbar.setTitle(R.string.note);
        createNoteToolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        createNoteToolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBackClick();
            }
        });
    }

    public void setListener() {
        createNoteAttachPicture.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onAttachClick();
            }
        });
        deletePhotoImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                createNotePhoto.setVisibility(GONE);
                deletePhotoImageView.setVisibility(GONE);
                uri = null;
            }
        });
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
        setListener();
    }

    @Override
    public NoteModel getCurrentModel() {
        if (createNoteTitle.getText().toString().isEmpty()
                && createNoteText.getText().toString().isEmpty()
                && uri == null) {
            return null;
        }
        NoteModel newModel = new NoteModel();
        if (!createNoteTitle.getText().toString().isEmpty()) {
            newModel.setTitle(createNoteTitle.getText().toString());
        }
        if (!createNoteText.getText().toString().isEmpty()) {
            newModel.setText(createNoteText.getText().toString());
        }
        if (createNotePhoto.getVisibility() == VISIBLE && uri != null) {
            newModel.setPictureUri(uri);
        }
        return newModel;
    }

    @Override
    public void setImage(Uri uri) {
        createNotePhoto.setImageURI(
                Uri.parse(
                        "file://" + uri.getLastPathSegment()
                )
        );
        createNotePhoto.setVisibility(VISIBLE);
        deletePhotoImageView.setVisibility(VISIBLE);
        this.uri = uri.toString();
    }
}
