package com.example.mynotes.presentation.feature.main.fragmentview;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mynotes.R;
import com.example.mynotes.dto.NoteModel;
import com.example.mynotes.presentation.feature.main.adapter.NotesAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainFragmentView extends ConstraintLayout implements IMainFragmentView {

    private EditText searchEditText;
    private TextView createTextView;
    private RecyclerView notesRecycler;
    private Listener listener;

    private List<NoteModel> models;

    public MainFragmentView(Context context) {
        super(context);
    }

    public MainFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MainFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
    }

    private void findViews() {
        searchEditText = findViewById(R.id.search_edit_text);
        createTextView = findViewById(R.id.create_edit_text);
        notesRecycler = findViewById(R.id.notes_recycler_view);
    }

    @Override
    public void populate(List<NoteModel> models) {
        this.models = models;
        NotesAdapter adapter = new NotesAdapter(models, listener);
        notesRecycler.setAdapter(adapter);
        setListeners();
    }

    private void setListeners() {
        createTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCreateNoteClick();
            }
        });
        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    notesRecycler.setAdapter(new NotesAdapter(models, listener));
                } else {
                    ArrayList<NoteModel> searchedModels = new ArrayList<>();
                    for (NoteModel model : models) {
                        if ((model.getTitle() != null && model.getTitle().startsWith(s.toString()))
                                || (model.getText() != null && model.getText().startsWith(s.toString()))) {
                            searchedModels.add(model);
                        }
                    }
                    notesRecycler.setAdapter(new NotesAdapter(searchedModels, listener));
                }
            }
        });
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
