package com.example.mynotes.presentation.feature.createnote.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.mynotes.R;
import com.example.mynotes.dto.NoteModel;
import com.example.mynotes.presentation.feature.createnote.fragmentview.ICreateNoteFragmentView;
import com.example.mynotes.presentation.utils.SaveDataUtils;

public class CreateNoteFragment extends Fragment {

    private ICreateNoteFragmentView view;
    private NoteModel model;

    public final static int PICK_IMAGE_CODE = 421;

    public static CreateNoteFragment getInstance(NoteModel model) {
        CreateNoteFragment fragment = new CreateNoteFragment();
        fragment.model = model;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.create_note_fragment_view, container, false);
        view = (ICreateNoteFragmentView) root;
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        view.setListener(new ICreateNoteFragmentView.Listener() {

            @Override
            public void onBackClick() {
                getActivity().onBackPressed();
            }

            @Override
            public void onAttachClick() {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);
                } else {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                    galleryIntent.setType("image/*");
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    galleryIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    getActivity().startActivityForResult(Intent.createChooser(galleryIntent, getString(R.string.choosing_image)), PICK_IMAGE_CODE);

                }
            }
        });
        view.initToolbar();
        if (model != null) {
            view.populate(model);
        }
    }

    public void save() {
        SaveDataUtils saveUtils = new SaveDataUtils(getContext());
        NoteModel currentModel = view.getCurrentModel();
        if (currentModel == null) {
            if (model != null) {
                saveUtils.deleteNote(model);
            }
            return;
        }
        if (model == null) {
            saveUtils.addNewNote(currentModel);
        } else {
            saveUtils.saveExisting(model, view.getCurrentModel());
        }
    }

    public void setImage(Uri selectedImage) {
        if (selectedImage != null) {
            view.setImage(selectedImage);
        }
    }
}
