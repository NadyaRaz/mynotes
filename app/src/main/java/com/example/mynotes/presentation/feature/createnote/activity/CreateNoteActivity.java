package com.example.mynotes.presentation.feature.createnote.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mynotes.R;
import com.example.mynotes.dto.NoteModel;
import com.example.mynotes.presentation.feature.createnote.fragment.CreateNoteFragment;

import static com.example.mynotes.presentation.feature.createnote.fragment.CreateNoteFragment.PICK_IMAGE_CODE;

public class CreateNoteActivity extends AppCompatActivity {

    private static NoteModel model;

    public static void startActivity(Context context, NoteModel mmodel) {
        model = mmodel;
        Intent intent = new Intent(context, CreateNoteActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, CreateNoteFragment.getInstance(model)).commit();
    }

    @Override
    public void onBackPressed() {
        ((CreateNoteFragment) getSupportFragmentManager().getFragments().get(0)).save();
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == PICK_IMAGE_CODE) {
            Uri selectedImage = data.getData();
            ((CreateNoteFragment) getSupportFragmentManager().getFragments().get(0)).setImage(selectedImage);
        }
    }
}
