package com.example.mynotes.presentation.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.mynotes.dto.NoteModel;
import com.example.mynotes.dto.NotesListModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class SaveDataUtils {

    private SharedPreferences prefs;
    private Context context;

    private final static String PREF_KEY = "MY_NOTES_PREFS_KEY";
    private final static String NOTES_LIST_KEY = "NOTES_LIST_KEY";

    public SaveDataUtils(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
    }

    public void saveNotesList(List<NoteModel> models) {
        Gson gson = new Gson();
        prefs.edit().putString(NOTES_LIST_KEY, gson.toJson(new NotesListModel(models))).apply();
    }

    public List<NoteModel> getNotesList() {
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            String listAtJson = prefs.getString(NOTES_LIST_KEY, "");
            return gsonBuilder.create().fromJson(listAtJson, NotesListModel.class).getModels();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public void addNewNote(NoteModel model) {
        List<NoteModel> models = getNotesList();
        models.add(0, model);
        saveNotesList(models);
    }

    public void saveExisting(NoteModel old, NoteModel n) {
        List<NoteModel> models = getNotesList();
        for (int i = 0; i < models.size(); i++) {
            if (models.get(i).equals(old)) {
                models.set(i, n);
                saveNotesList(models);
                return;
            }
        }
    }

    public void deleteNote(NoteModel model) {
        List<NoteModel> models = getNotesList();
        for (int i = 0; i < models.size(); i++) {
            if (models.get(i).equals(model)) {
                models.remove(i);
                saveNotesList(models);
                return;
            }
        }
    }
}
