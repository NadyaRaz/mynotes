package com.example.mynotes.presentation.feature.main.fragmentview;

import com.example.mynotes.dto.NoteModel;

import java.util.List;

public interface IMainFragmentView {

    void populate(List<NoteModel> models);

    void setListener(Listener listener);

    interface Listener {

       void onCreateNoteClick();

       void onOpenNoteClick(NoteModel model);

       void onDeleteClick(NoteModel model);
    }
}
