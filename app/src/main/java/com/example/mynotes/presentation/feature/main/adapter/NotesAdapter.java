package com.example.mynotes.presentation.feature.main.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mynotes.R;
import com.example.mynotes.dto.NoteModel;

import java.util.List;

import static com.example.mynotes.presentation.feature.main.fragmentview.IMainFragmentView.Listener;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.Holder> {

    private List<NoteModel> models;
    private Listener listener;

    public NotesAdapter(List<NoteModel> models, Listener listener) {
        this.models = models;
        this.listener = listener;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.populate(models.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        private TextView textView;
        private TextView titleView;
        private ImageView imageView;
        private View view;

        public Holder(@NonNull View itemView) {
            super(itemView);
            titleView = itemView.findViewById(R.id.note_item_title);
            textView = itemView.findViewById(R.id.note_item_text);
            imageView = itemView.findViewById(R.id.note_item_image);
            view = itemView;
        }

        void populate(final NoteModel model, final Listener listener) {
            if (model.getTitle() == null) {
                titleView.setVisibility(View.GONE);
            } else {
                titleView.setVisibility(View.VISIBLE);
                titleView.setText(model.getTitle());
            }
            if (model.getText() == null) {
                textView.setVisibility(View.GONE);
            } else {
                textView.setVisibility(View.VISIBLE);
                textView.setText(model.getText());
            }
            if (model.getPictureUri() == null) {
                imageView.setVisibility(View.GONE);
            } else {
                imageView.setImageURI(
                        Uri.parse(
                                "file://" + Uri.parse(model.getPictureUri()).getLastPathSegment()
                        )
                );
                imageView.setVisibility(View.VISIBLE);
            }
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onOpenNoteClick(model);
                }
            });
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onDeleteClick(model);
                    return false;
                }
            });
        }
    }
}
