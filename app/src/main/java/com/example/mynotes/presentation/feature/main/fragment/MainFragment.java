package com.example.mynotes.presentation.feature.main.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.mynotes.R;
import com.example.mynotes.dto.NoteModel;
import com.example.mynotes.presentation.feature.createnote.activity.CreateNoteActivity;
import com.example.mynotes.presentation.feature.main.fragmentview.IMainFragmentView;
import com.example.mynotes.presentation.utils.SaveDataUtils;

public class MainFragment extends Fragment {

    private IMainFragmentView view;

    public static MainFragment getInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.main_fragment_view, container, false);
        view = (IMainFragmentView) root;
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        view.setListener(new IMainFragmentView.Listener() {

            @Override
            public void onCreateNoteClick() {
                CreateNoteActivity.startActivity(getContext(), null);
            }

            @Override
            public void onOpenNoteClick(NoteModel model) {
                CreateNoteActivity.startActivity(getContext(), model);
            }

            @Override
            public void onDeleteClick(final NoteModel model) {
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.delete_title)
                        .setMessage(R.string.delete_message)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SaveDataUtils saveUtils = new SaveDataUtils(getContext());
                                saveUtils.deleteNote(model);
                                view.populate(saveUtils.getNotesList());
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        SaveDataUtils saveUtils = new SaveDataUtils(getContext());
        view.populate(saveUtils.getNotesList());
    }
}
