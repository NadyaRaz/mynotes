package com.example.mynotes.presentation.feature.createnote.fragmentview;

import android.net.Uri;

import com.example.mynotes.dto.NoteModel;

public interface ICreateNoteFragmentView {

    void populate(NoteModel model);

    void initToolbar();

    void setListener(Listener listener);

    NoteModel getCurrentModel();

    void setImage(Uri uri);

    interface Listener {

        void onBackClick();
        
        void onAttachClick();
    }
}
