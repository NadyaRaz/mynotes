package com.example.mynotes.presentation.feature.main.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.mynotes.R;
import com.example.mynotes.dto.NoteModel;
import com.example.mynotes.presentation.feature.main.fragment.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, MainFragment.getInstance()).commit();
    }
}
