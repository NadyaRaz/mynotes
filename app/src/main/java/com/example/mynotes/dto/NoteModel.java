package com.example.mynotes.dto;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class NoteModel {

    private String pictureUri;
    private String title;
    private String text;

    public String getPictureUri() {
        return pictureUri;
    }

    public void setPictureUri(String pictureUri) {
        this.pictureUri = pictureUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        try {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            NoteModel o = (NoteModel) obj;
            return this.toString().equals(o.toString());
        } catch (Exception e) {
            return false;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return title + ";" + text + ";" + pictureUri;
    }
}
