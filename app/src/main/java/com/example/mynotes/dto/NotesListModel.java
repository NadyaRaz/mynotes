package com.example.mynotes.dto;

import java.util.List;

public class NotesListModel {

    private List<NoteModel> models;

    public NotesListModel(List<NoteModel> models) {
        this.models = models;
    }

    public List<NoteModel> getModels() {
        return models;
    }

    public void setModels(List<NoteModel> models) {
        this.models = models;
    }
}
